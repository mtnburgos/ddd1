package net.jbl.pos;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by jorgeburgos on 8/16/15.
 */
public class MyMathContext {

    public static void main(String[] args) {
        BigDecimal bg1 = new BigDecimal("12345.46479");

        MathContext mc = new MathContext(10);

        BigDecimal bg2 = bg1.round(mc);

        String str = "The value " + bg1 + " after rounding is " + bg2;

        // print bg2 value
        System.out.println(str);
    }
}
