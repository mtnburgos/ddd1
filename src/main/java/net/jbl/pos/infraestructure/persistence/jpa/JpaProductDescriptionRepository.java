package net.jbl.pos.infraestructure.persistence.jpa;

import net.jbl.pos.domain.model.product.ProductDescription;
import net.jbl.pos.domain.model.product.ProductDescriptionId;
import net.jbl.pos.domain.model.product.ProductDescriptionRepository;

import javax.enterprise.context.ApplicationScoped;
import java.io.Serializable;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@ApplicationScoped
public class JpaProductDescriptionRepository extends JpaRepository implements ProductDescriptionRepository, Serializable {


    public ProductDescription findById(ProductDescriptionId productDescriptionId) {
        return entityManager.find(ProductDescription.class, productDescriptionId);
    }

    @Override
    public ProductDescription findById(String productDescriptionId) {
        ProductDescriptionId prodDescId = new ProductDescriptionId(productDescriptionId);
        return findById(prodDescId);

    }

    @Override
    public void store(ProductDescription productDescription) {
        entityManager.persist(productDescription);
    }
}
