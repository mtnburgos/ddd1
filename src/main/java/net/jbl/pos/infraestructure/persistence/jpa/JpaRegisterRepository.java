package net.jbl.pos.infraestructure.persistence.jpa;

import net.jbl.pos.domain.model.register.ProductDescription;
import net.jbl.pos.domain.model.register.RegisterRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@ApplicationScoped
public class JpaRegisterRepository implements RegisterRepository {
    @Override
    public ProductDescription findProductDescriptionById(String productDescriptionId) {
        return null;
    }
}
