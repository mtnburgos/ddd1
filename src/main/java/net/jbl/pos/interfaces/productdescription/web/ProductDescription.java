package net.jbl.pos.interfaces.productdescription.web;

import net.jbl.pos.interfaces.productdescription.facade.ProductDescriptionServiceFacade;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@Named
@RequestScoped
public class ProductDescription {

    private String name;
    private String price;
    private String currency;

    @Inject
    private ProductDescriptionServiceFacade prodDescServiceFacade;

    @PostConstruct
    public void init() {

    }

    public void register() {
        prodDescServiceFacade.store(name, price, currency);
    }

}
