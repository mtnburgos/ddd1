package net.jbl.pos.interfaces.productdescription.facade;

import net.jbl.pos.domain.model.product.ProductDescription;

import java.util.List;

/**
 * Created by jorgeburgos on 8/17/15.
 */
public interface ProductDescriptionServiceFacade {
    List<ProductDescription> listAllProductDescriptions();

    public void store(String name, String price, String currency);
}
