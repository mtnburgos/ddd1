package net.jbl.pos.interfaces.productdescription.facade.internal;

import net.jbl.pos.application.ProductDescriptionService;
import net.jbl.pos.domain.model.product.ProductDescription;
import net.jbl.pos.domain.model.product.ProductDescriptionRepository;
import net.jbl.pos.interfaces.productdescription.facade.ProductDescriptionServiceFacade;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@ApplicationScoped
public class DefaultProductDescriptionServiceFacade implements ProductDescriptionServiceFacade, Serializable {

    @Inject
    private ProductDescriptionService productDescriptionService;

    @Inject
    private ProductDescriptionRepository productDescriptionRepository;

    @Override
    public List<ProductDescription> listAllProductDescriptions() {
        return null;
    }

    @Override
    public void store(String name, String price, String currency) {
        productDescriptionService.create(name, price, currency);
    }
}
