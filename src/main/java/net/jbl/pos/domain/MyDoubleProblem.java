package net.jbl.pos.domain;

import java.math.BigDecimal;

/**
 * Created by jorgeburgos on 8/16/15.
 */
public class MyDoubleProblem {
    public static void main(String[] args) {
        double unCentavo = 0.1;
        double suma = unCentavo + unCentavo +unCentavo + unCentavo +unCentavo + unCentavo+ unCentavo+ unCentavo;
        System.out.println(suma);


        BigDecimal unCent = new BigDecimal("0.1");
        BigDecimal sum = unCent.add(unCent).add(unCent).add(unCent).add(unCent).add(unCent).add(unCent).add(unCent);
        System.out.println(sum);
    }
}
