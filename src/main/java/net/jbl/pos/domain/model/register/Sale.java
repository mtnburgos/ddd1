package net.jbl.pos.domain.model.register;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@Entity
public class Sale {

    private Date time;
    private boolean isComplete;

    private List<SalesLineItem> lineItems = new ArrayList<>();


    public void makeLineItem(ProductDescription productDescription, int quantity) {
        lineItems.add(new SalesLineItem(productDescription, quantity, this));
    }


    public static Sale Create() {

        Sale sale = new Sale();
        sale.time = new Date();
        sale.isComplete = false;

        return sale;

    }

//    public void makeLineItem()
}
