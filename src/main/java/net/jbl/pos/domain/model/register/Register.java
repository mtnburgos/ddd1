package net.jbl.pos.domain.model.register;


/**
 * Created by jorgeburgos on 8/17/15.
 */
public class Register {

    private Sale currentSale;

    public void makeNewSale() {
        currentSale = new Sale();
    }

    public void makeLineItem(ProductDescription productDescription, int quantity){
        currentSale.makeLineItem(productDescription, quantity);
    }

}
