package net.jbl.pos.domain.model.product;

/**
 * Created by jorgeburgos on 8/17/15.
 */
public interface ProductDescriptionRepository {

    ProductDescription findById(String productDescriptionId);

    void store(ProductDescription productDescription);
}
