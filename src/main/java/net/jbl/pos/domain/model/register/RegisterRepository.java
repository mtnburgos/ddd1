package net.jbl.pos.domain.model.register;

/**
 * Created by jorgeburgos on 8/17/15.
 */
public interface RegisterRepository {
    ProductDescription findProductDescriptionById(String productDescriptionId);
}
