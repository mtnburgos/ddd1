package net.jbl.pos.domain.model.register;

import net.jbl.pos.domain.shared.currency.Money;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@Entity
public class SalesLineItem {

    private int quantity;
    private String productDescriptionId;
    private Sale sale;

    public SalesLineItem(ProductDescription productDescription, int quantity, Sale sale) {
        this.productDescription = productDescription;
        this.productDescriptionId = productDescription.getId().getIdString();

        this.quantity = quantity;
        this.sale = sale;
    }

    public Money getSubtotal() {
        return productDescription.getPrice().times(quantity);
    }

    @Transient
    private ProductDescription productDescription;
}
