package net.jbl.pos.domain.model.register;

import net.jbl.pos.domain.model.product.ProductDescriptionId;
import net.jbl.pos.domain.shared.currency.Money;

import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 * Created by jorgeburgos on 8/16/15.
 */
@Entity
public class ProductDescription {

    @Embedded
    ProductDescriptionId id;

    private String name;

    @Embedded
    private Money price;

    protected ProductDescription() {}

    public ProductDescriptionId getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public Money getPrice() {
        return price;
    }

}
