package net.jbl.pos.domain.model.product;

import org.apache.commons.lang3.Validate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by jorgeburgos on 8/17/15.
 */

@Embeddable
public class ProductDescriptionId implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @Column(name = "id", unique = true, updatable = false)
    private String id;

    public ProductDescriptionId() {}

    public ProductDescriptionId(String id) {
        Validate.notNull(id);

        this.id = id;
    }

    public String getIdString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductDescriptionId productDescriptionId = (ProductDescriptionId) o;

        return sameValueAs(productDescriptionId);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean sameValueAs(ProductDescriptionId productDescriptionId) {
        return id != null && id.equals(productDescriptionId.id);
    }

    @Override
    public String toString() {
        return id;
    }
}
