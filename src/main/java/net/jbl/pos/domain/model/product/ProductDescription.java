package net.jbl.pos.domain.model.product;

import net.jbl.pos.domain.shared.currency.Money;
import org.apache.commons.lang3.Validate;

import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@Entity
public class ProductDescription {


    @Embedded
    ProductDescriptionId id;

    private String name;

    @Embedded
    private Money price;

    protected ProductDescription() {}

    /**
     *
     * @param name Product name
     * @param price Product price
     * @throws IllegalArgumentException if name or price is null
     */
    public ProductDescription(String name, Money price) {
        Validate.notNull(name);
        Validate.notNull(price);

        this.name = name;
        this.price = price;
    }

    public ProductDescription(String name, String price, String currency) {
        this(name, new Money(price, currency));
    }

    /**
     *
     * @param name Product name
     * @param price Product price
     * @param id
     */
    public ProductDescription(String name, Money price, String id) {
        Validate.notNull(name);
        Validate.notNull(price);
        Validate.notNull(id);

        this.name = name;
        this.price = price;
        this.id = new ProductDescriptionId(id);
    }


    public ProductDescriptionId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }
}
