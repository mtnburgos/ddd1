package net.jbl.pos.domain.shared.currency;

/**
 * Created by jorgeburgos on 8/16/15.
 */
public enum  Currency {
    BS("Bs"), USD("USD"), EUR("EUR");

    private String code;

    private Currency(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
