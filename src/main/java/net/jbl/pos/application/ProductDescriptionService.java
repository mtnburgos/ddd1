package net.jbl.pos.application;

/**
 * Created by jorgeburgos on 8/17/15.
 */
public interface ProductDescriptionService {

    public void create(String name, String price, String currency);

    public void update(String productDescriptionId, String name, String price);

    public void delete(String productDescriptionId);

}
