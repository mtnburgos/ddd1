package net.jbl.pos.application.internal;

import net.jbl.pos.application.ProductDescriptionService;
import net.jbl.pos.domain.model.product.ProductDescriptionRepository;
import net.jbl.pos.domain.model.product.ProductDescription;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by jorgeburgos on 8/17/15.
 */
@Stateless
public class DefaultProductDescriptionService implements ProductDescriptionService {

    @Inject
    ProductDescriptionRepository productDescriptionRepository;

    @Override
    public void create(String name, String price, String currency) {
        ProductDescription productDescription = new ProductDescription(name, price, currency);
        productDescriptionRepository.store(productDescription);
    }

    @Override
    public void update(String productDescriptionId, String name, String price) {

    }

    @Override
    public void delete(String productDescriptionId) {

    }



}
