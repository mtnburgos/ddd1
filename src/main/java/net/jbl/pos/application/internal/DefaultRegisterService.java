package net.jbl.pos.application.internal;

import net.jbl.pos.application.RegisterService;
import net.jbl.pos.domain.model.register.RegisterRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by jorgeburgos on 8/16/15.
 */

@Stateless
public class DefaultRegisterService implements RegisterService {


//    @Inject
//    Register register;

    @Inject
    RegisterRepository registerRepository;


    @Override
    public void makeNewSale() {
//        register.makeNewSale();

    }

    @Override
    public void enterItem(String productDescriptionId, int quantity) {
//        ProductDescription productDescription = registerRepository.findProductDescriptionById(productDescriptionId);
//        register.makeLineItem(productDescription, quantity);
    }

    @Override
    public void makePayment() {

    }

    @Override
    public void endSale() {

    }
}
