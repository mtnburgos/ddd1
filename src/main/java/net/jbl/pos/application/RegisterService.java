package net.jbl.pos.application;

/**
 * Created by jorgeburgos on 8/16/15.
 */

/**
 *
 */
public interface RegisterService {

    public void makeNewSale();

    public void enterItem(String productDescriptionId, int quantity);

    public void makePayment();

    public void endSale();




}
