package net.jbl.pos.domain.share.currency;

import net.jbl.pos.domain.shared.currency.Currency;
import net.jbl.pos.domain.shared.currency.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.RoundingMode;

/**
 * Created by jorgeburgos on 8/16/15.
 */
public class MoneyTest {

    @Before
    public void setUp() {
        Money.init(Currency.BS, RoundingMode.HALF_EVEN);
    }

    @Test
    public void toStringTests() {
        Money usd = new Money(20, Currency.USD);
        Money bs = new Money(60L, Currency.BS);
        Money eur = new Money(31.7777, Currency.EUR);

        Assert.assertEquals("20 USD", usd.toString());
        Assert.assertEquals("60 Bs", bs.toString());
        Assert.assertEquals("31.78 EUR", eur.toString());
    }

    @Test
    public void valueTest() {

    }

    @Test
    public void negativePositiveAddition() {
        Money augend = new Money("-1.20", Currency.BS);
        Money addend = new Money("1.01", Currency.BS);
        Money result = augend.plus(addend);

        Money expected = new Money("-0.19", Currency.BS);
        Assert.assertEquals(expected, result);

    }

    @Test
    public void positiveNegativeAddition() {
        Money augend = new Money(2.01, Currency.EUR);
        Money addend = new Money(-4.01, Currency.EUR);
        Money result = augend.plus(addend);

        Money expected = new Money(-2.00, Currency.EUR);
        Assert.assertEquals(expected, result);

    }

    @Test(expected = Money.MismatchedCurrencyException.class)
    public void incompatibleCurrencyAdditionFromBsToUsd() {
        Money augend = new Money("1.99", Currency.BS);
        Money addend = new Money("1.01", Currency.USD);

        augend.plus(addend);

    }
}
